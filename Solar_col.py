# -*- coding: utf-8 -*-
"""
Created on Mon Aug 21 11:07:16 2023

@author: rpiso
"""


import numpy as np
import math
import pandas as pd

#from __future__ import division


def Solar_col_gen(direc,weathercase,col_temperature,eta_0,a1,a2):
    # Load CSV files
    GI = pd.read_csv(direc + 'DRY_global_radiation_hourly_' + str(weathercase) + '.csv', sep=';')
    DHI = pd.read_csv(direc + 'DRY_diffuse_irradiance_hourly_' + str(weathercase) + '.csv', sep=';')
    ambient_temperature=pd.read_csv(direc + 'DRY_temperature_hourly_6060' + '.csv', sep=';')['temperature_mean(C)']
    # Location parameters
    longitude = 12.568337
    latitude = 55.676098
    LSTM = 1 * 15
    slope = 30
    azimuth = 180
    
    # Calculate irradiance
    df = pd.DataFrame(columns=['Day', 'Time', 'GI', 'DI'])
    df['GI'] = GI['radiation_mean(W/m^2)']
    df['DI'] = DHI['Diffuse_irradiance_[W/m^2]']
    for i in range(len(df)):
        df.loc[i, 'Day'] = math.floor(i/24) + 1
        df.loc[i, 'Time'] = i % 24
    B = 360/365 * (df['Day'] - 81)
    EoT= [9.87 * math.sin(2 * math.radians(b)) - 7.53 * math.cos(math.radians(b)) - 1.5 * b for b in B]
    TC = np.zeros(len(df))
    for i in range(len(df)):
        TC[i] = 4 * (longitude - LSTM) + EoT[i]
    HRA = 15 * (df['Time'] + TC/60 - 12)
    decl = np.zeros(len(df))
    elev = np.zeros(len(df))
    for i in range(len(df)):
        decl[i] = -23.45 * math.cos(math.radians(360/365 * (df['Day'][i] + 10)))
        elev[i] = math.degrees(math.asin(math.sin(math.radians(decl[i])) * math.sin(math.radians(latitude))
                   + math.cos(math.radians(decl[i])) * math.cos(math.radians(latitude)) * math.cos(math.radians(HRA[i]))))
    
    Direct = df['GI'] - df['DI']
    
    G= np.zeros(len(df))
    Solar_output=np.zeros(len(df))
    for i in range(len(df)):
        G[i] = df['DI'][i] * (180 - slope)/180 + Direct[i] * (math.sin(math.radians(decl[i])) * math.sin(math.radians(latitude))
                                                   * math.cos(math.radians(slope))
                                                   - math.sin(math.radians(decl[i])) * math.cos(math.radians(latitude))
                                                   * math.sin(math.radians(slope)) * math.cos(math.radians(azimuth))
                                                   + math.cos(math.radians(decl[i])) * math.cos(math.radians(latitude))
                                                   * math.cos(math.radians(slope)) * math.cos(math.radians(HRA[i]))
                                                   + math.cos(math.radians(decl[i])) * math.sin(math.radians(latitude))
                                                   * math.sin(math.radians(slope)) * math.cos(math.radians(azimuth))
                                                   * math.cos(math.radians(HRA[i])) + math.cos(math.radians(decl[i]))
                                                   * math.cos(math.radians(latitude)) * math.cos(math.radians(slope))
                                                   * math.cos(math.radians(HRA[i])) + math.cos(math.radians(decl[i]))
                                                   * math.sin(math.radians(azimuth)) * math.sin(math.radians(HRA[i]))
                                                   * math.sin(math.radians(slope)))
        if G[i]>0:
            Solar_output[i]=G[i]*(eta_0-a1*(col_temperature-ambient_temperature[i])/G[i]-a2*(col_temperature-ambient_temperature[i])*(col_temperature-ambient_temperature[i])/G[i])
        else:
            Solar_output[i]=0
    return Solar_output,G

col_temperature=60
weathercase=6031
eta_0=0.812
a1=3.478
a2=0.018

direc='C:/Users/rpiso/Desktop/PhD_Python_files/Solar_Radiation_On_Tilted_Surface/'
Solar_output,G=Solar_col_gen(direc,weathercase,col_temperature,eta_0,a1,a2)

print(Solar_output)
