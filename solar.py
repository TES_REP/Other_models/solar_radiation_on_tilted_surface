# -*- coding: utf-8 -*-
"""
Created on Fri Oct 28 15:46:35 2022

@author: rpiso
"""

import numpy as np
import math
import pandas as pd

#from __future__ import division

def PVoutput(G,PVpeak):
    
    PVoutput=G/1000*PVpeak
    return PVoutput

def Thermal_col_output(G,ambientT,T_col,eta0,a1,a2,Area):
    Thermal_col_eff=np.zeros(len(G))
    Thermal_col_output=np.zeros(len(G))
    for i in range (len(G)):
        if G[i]>0:
            Thermal_col_eff[i]=eta0-a1*(T_col-ambientT[i])/G[i]-a2*(T_col-ambientT[i])*(T_col-ambientT[i])/G[i]
            if Thermal_col_eff[i]>0:
                Thermal_col_output[i]=G[i]*Thermal_col_eff[i]*Area
    return Thermal_col_output

def Radiation(GI,DHI,location_parameters):
    
    # Location parameters
    longitude = location_parameters[0]
    latitude = location_parameters[1]
    LSTM = location_parameters[2]
    slope = location_parameters[3]
    azimuth = location_parameters[4]
    
    # Calculate irradiance
    df = pd.DataFrame(columns=['Day', 'Time', 'GI', 'DI'])
    df['GI'] = GI['radiation_mean(W/m^2)']
    df['DI'] = DHI['Diffuse_irradiance_[W/m^2]']
    for i in range(len(df)):
        df.loc[i, 'Day'] = math.floor(i/24) + 1
        df.loc[i, 'Time'] = i % 24
    B = 360/365 * (df['Day'] - 81)
    EoT= [9.87 * math.sin(2 * math.radians(b)) - 7.53 * math.cos(math.radians(b)) - 1.5 * b for b in B]
    TC = np.zeros(len(df))
    for i in range(len(df)):
        TC[i] = 4 * (longitude - LSTM) + EoT[i]
    HRA = 15 * (df['Time'] + TC/60 - 12)
    decl = np.zeros(len(df))
    elev = np.zeros(len(df))
    for i in range(len(df)):
        decl[i] = -23.45 * math.cos(math.radians(360/365 * (df['Day'][i] + 10)))
        elev[i] = math.degrees(math.asin(math.sin(math.radians(decl[i])) * math.sin(math.radians(latitude))
                   + math.cos(math.radians(decl[i])) * math.cos(math.radians(latitude)) * math.cos(math.radians(HRA[i]))))
    
    Direct = df['GI'] - df['DI']
    
    G= np.zeros(len(df))
    for i in range(len(df)):
        G[i] = df['DI'][i] * (180 - slope)/180 + Direct[i] * (math.sin(math.radians(decl[i])) * math.sin(math.radians(latitude))
                                                   * math.cos(math.radians(slope))
                                                   - math.sin(math.radians(decl[i])) * math.cos(math.radians(latitude))
                                                   * math.sin(math.radians(slope)) * math.cos(math.radians(azimuth))
                                                   + math.cos(math.radians(decl[i])) * math.cos(math.radians(latitude))
                                                   * math.cos(math.radians(slope)) * math.cos(math.radians(HRA[i]))
                                                   + math.cos(math.radians(decl[i])) * math.sin(math.radians(latitude))
                                                   * math.sin(math.radians(slope)) * math.cos(math.radians(azimuth))
                                                   * math.cos(math.radians(HRA[i])) + math.cos(math.radians(decl[i]))
                                                   * math.cos(math.radians(latitude)) * math.cos(math.radians(slope))
                                                   * math.cos(math.radians(HRA[i])) + math.cos(math.radians(decl[i]))
                                                   * math.sin(math.radians(azimuth)) * math.sin(math.radians(HRA[i]))
                                                   * math.sin(math.radians(slope)))
    

   
    return(G)


PVpeak=12000 #kW
weathercase=6031
direc='C:/Users/rpiso/Desktop/PhD_Python_files/Solar_Radiation_On_Tilted_Surface/'

# Load CSV files
GI = pd.read_csv(direc + 'DRY_global_radiation_hourly_' + str(weathercase) + '.csv', sep=';')
DHI = pd.read_csv(direc + 'DRY_diffuse_irradiance_hourly_' + str(weathercase) + '.csv', sep=';')

# Location parameters
longitude = 12.568337
latitude = 55.676098
LSTM = 1 * 15
slope = 30
azimuth = 180
location_parameters=[longitude,latitude,LSTM,slope,azimuth]


G=Radiation(GI,DHI,location_parameters)


